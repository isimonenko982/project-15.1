#include <iostream> 

using namespace std;

void printOddNumbers(int n, int* a) {
	int k = 0;
	for (int i = 0; i < n; i++) {
		if (a[i] % 2 != 0) {
			k = 1;
			cout << a[i] << " ";
		}
	}
	if (k == 0) {
		cout << "Array doesn't contain odd numbers";
	}
	cout << "\n";
}

int main() {
	int n;
	cout << "Enter the size of the array: ";
	cin >> n;

	int* a = new int[n];
	cout << "Enter " << n << " numbers:\n";
	for (int i = 0; i < n; i++) {
		cin >> a[i];
	}

	printOddNumbers(n, a);

	delete[] a;
	system("pause");
	return 0;
}